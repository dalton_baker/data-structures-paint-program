/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the rectangle class
 *****************************************************************************/
#include "rectangle.h"


/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the rectangle constructor
 *
 * @param[in] p - point where the rectangle will be drawn from
 *
 * @param[in] h - the height of the rectangle
 *
 * @param[in] w - the width of the rectangle
 *
 * @param[in] color - the index of the color used for the rectangle
 *
 *****************************************************************************/
Rectangle::Rectangle(Point p, float h, float w, int color) : Shape(p, h, w, color)
{
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * This is will draw a rectangle to the screen
 * Most of this code was provide, but I did tweak it a lot
 *
 *****************************************************************************/
void Rectangle::draw()
{
   //first we need to calculate x1, x2, y1, & y2 using our stored values
   float x1 = location.returnX();
   float x2 = location.returnX() + width;
   float y1 = glutGet(GLUT_WINDOW_HEIGHT) - location.returnY();
   float y2 = glutGet(GLUT_WINDOW_HEIGHT) - location.returnY() - height;

      glLineWidth( 2 );
   glColor3fv( colors[shapeColor] );
   glBegin( GL_LINE_LOOP );
      glVertex2f( x1, y1 );
      glVertex2f( x2, y1 );
      glVertex2f( x2, y2 );
      glVertex2f( x1, y2 );
   glEnd();
   glFlush();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will tell you if a rectangle contains a point
 *
 * @param[in] p - point to check the location of
 *
 * @return - a bool telling you if the point is in the shape
 *
 *****************************************************************************/
bool Rectangle::contains(Point p)
{
   //height and width are positive
   if(height > 0 && width > 0 )
   {
      if(p.returnX() < (location.returnX() + width) &&
        (p.returnX() > location.returnX()) &&
        (p.returnY() < (location.returnY() + height)) &&
        (p.returnY() > location.returnY()))
        {
           return true;
        }
   }

   //height is positive & width is negative
   else if(height > 0 && width < 0 )
   {
      if(p.returnX() > (location.returnX() + width) &&
        (p.returnX() < location.returnX()) &&
        (p.returnY() < (location.returnY() + height)) &&
        (p.returnY() > location.returnY()))
        {
           return true;
        }
   }

   //height is negative & width is positive
   else if(height < 0 && width > 0)
   {
      if(p.returnX() < (location.returnX() + width) &&
        (p.returnX() > location.returnX()) &&
        (p.returnY() > (location.returnY() + height)) &&
        (p.returnY() < location.returnY()))
        {
           return true;
        }
   }

   //both height and width are negative
   else
   {
      if(p.returnX() > (location.returnX() + width) &&
        (p.returnX() < location.returnX()) &&
        (p.returnY() > (location.returnY() + height)) &&
        (p.returnY() < location.returnY()))
        {
           return true;
        }
   }

   //return false if it doesn't meet the criteria
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return a -1, indicating that this is not a filled object
 *
 * @return - -1
 *
 *****************************************************************************/
int Rectangle::getFillColor()
{
   return -1;
}

/**************************************************************************//**
 * @file
 * @brief Header file to be used with rectangle.cpp
 *****************************************************************************/
#ifndef __RECTANGLE__H__
#define __RECTANGLE__H__

#include "shape.h"

/*!
* @brief A class for the rectangle shape
*/
class Rectangle : public Shape
{
public:
   Rectangle(Point p, float h, float w, int color);
   void draw();
   bool contains(Point p);
   int getFillColor();
};

#endif

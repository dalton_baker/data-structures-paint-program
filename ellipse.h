/**************************************************************************//**
 * @file
 * @brief Header file to be used with ellipse.cpp
 *****************************************************************************/
#ifndef __ELLIPSE__H__
#define __ELLIPSE__H__

#include "shape.h"

/*!
* @brief A class for the ellipse shape
*/
class Ellipse : public Shape
{
public:
   Ellipse(Point p, float h, float w, int color);
   void draw();
   bool contains(Point p);
   int getFillColor();
};

#endif

/**************************************************************************//**
 * @file
 * @brief Start of the program, only contains main.
 ****************************************************************************/

/**************************************************************************//**
 * @file
 *
 * @mainpage Project 1 - Paint
 *
 * @section course_section Course Information
 *
 * @authors Dalton Baker & Rob Minick
 *
 * @date 2 / 11 / 2019
 *
 * @par Professor:
 *         Dr. Paul Hinker
 *
 * @par Course:
 *         CSC 315 - Section M001 - 10:00AM
 *
 * @par Location:
 *         McLaury - Room 313
 *
 * @section program_section Program Information
 *
 * @details This program will allow a user to draw several different shapes
 * onto a pallette and then move them around if they wish. The shapes that
 * will be available to draw are a line, a rectangle, an ellipse, a filled
 * rectangle, and a filled ellipse.
 *
 *    This program is implemented heavily using object oriented programming.
 * The first type of object is the event class. This is the class called
 * when events happen in the program. There is an Event base class and 4 event
 * types that inherit from it, the resize event, the keyboard event, the mouse
 * click event, and the keyboard event. The next type of object is the shape
 * class. This is the class that holds information for any shape drawn to the
 * screen. It has the base class Shape, and then the 5 types of shapes inherit
 * from it.
 *
 *    This porgram also uses a Point class to keep track of where on the screen
 * things are happening. The Point class has several overloaded operators,
 * making it much easier to implement. This program uses Glut to open a window
 * for the user to interface with the program. As a final note, there are
 * there are several keys that will allow the user to delete shapes and close
 * the program. The esc key and the 'q' key close the program, the 'd' key
 * deletes the top shape drawn by the user, the 'c' key clears all of the
 * shapes drawn by the user, and any other key will redraw the screen.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      You must be in program dirrectory!!!
 *      make
 *
 * @par Usage:
 * No special usage requirements.
   @verbatim
   user@linux:~/csc315_sp2019_project1 $ ./paint
   user@linux:~$ /path_to_dir/paint
   @endverbatim
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug Nothing known currently
 *
 * @todo Everything is finished, program should be fully functional
 *
 * @par Modifications and Development Timeline:
 * <a href="https://gitlab.mcs.sdsmt.edu/7032425/csc315_sp2019_project1">
 * Click Here </a> for a list of all contributions made to this program.
 *
 ****************************************************************************/
#include <GL/freeglut.h>
#include "callbacks.h"

void oneTimeInit(int num, char **arrOfStr);

/**************************************************************************//**
 * @author Dr. Paul Hinker
 *
 * @par Description:
 * This is the main function, the program starts here.
 * This is basically exactly what Dr. Paul Hinker's main function looked like.
 *
 * @param[in] argc - The number of arguments provided to the program
 *
 * @param[in] argv - an array containing the arguments provided
 *
 * @returns 0 The program ran with no errors.
 *
 * @returns 1 Something went wrong in the program.
 *
 *****************************************************************************/
int main(int argc, char** argv)
{
   // Call the glut functions to initialize everything
   oneTimeInit(argc, argv);
   glutMainLoop();
   return 0;
}

/**************************************************************************//**
 * @author Dr. Paul Hinker
 *
 * @brief A function to call the usual set of initialization routines
 *
 * @par Description
 *   * Initializes the glut system
 *   * Sets the initial display mode
 *   * Sets the initial window size
 *   * Sets the initial window position
 *   * Creates the window and sets the window title
 *   * Sets the DisplayFunc callback
 *   * Sets the KeyboardFunc callback
 *   * Sets the MouseFunc callback
 *   * Sets the clear color
 *
 * @param[in] num      - The number of entries in the arrOfStr parameter
 * @param[in] arrOfStr - The commandline arguments
 *
 *****************************************************************************/
void oneTimeInit(int num, char **arrOfStr)
{
    // Initialize GLUT (OpenGL Utility Toolkit)
    glutInit(&num, arrOfStr);

    // Set the display mode [double buffering, 4-channel red,green,blue,alpha]
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA);

    // Make the window 640 columns x 480 rows of pixels
    glutInitWindowSize(640, 480);

    // Start the window at 100 pixels down and 100 pixels right of upper-left
    glutInitWindowPosition(100, 100);

    // Give the window a title
    if (num > 1)
       glutCreateWindow(arrOfStr[1]);
    else
       glutCreateWindow("Dalton Baker & Robert Minick");

    // Set the function for the glutMainLoop to call when refreshing window
    glutDisplayFunc(display);

    // Set the function to call in response to keyboard evenDts
    glutKeyboardFunc(keyboard);

    // Set the function to call for mouse Events
    glutMouseFunc(mouseClick);

    // Set the function to call for reshaping of the window
    glutReshapeFunc(reshape);

    //set function to call for when the window is closed
    glutCloseFunc(onClose);
    // When the window is cleared, use the color that's 0% red, 100% green,
    // 0% blue, 0% alpha
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

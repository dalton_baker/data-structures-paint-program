/**************************************************************************//**
 * @file
 * @brief Header file to be used with displayEvent.cpp
 *****************************************************************************/
#ifndef __DISPLAYEVENT__H__
#define __DISPLAYEVENT__H__

#include "event.h"

/*!
* @brief A class for when the display changes
*/
class DisplayEvent : public Event
{
public:
   void processEvent();
};

#endif

/**************************************************************************//**
 * @file
 * @brief Callback functions for glut
 *****************************************************************************/
#include "callbacks.h"

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker
 *
 * @par Description:
 * This function gets called every time an Event class is called
 * Most of this function was taken dirrectly from Dr. Hinkers code
 *
 * @param[in] event - the event that we will be processing
 *
 *****************************************************************************/
void universalEventProcessor(Event *event)
{
   //process the event in the object
   event->processEvent();

   //delete the object
   delete event;
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * Function for the display callback
 *
 *****************************************************************************/
void display()
{
    universalEventProcessor(new DisplayEvent());
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Function for the keyboard callback
 *
 * @param[in] key - the key that was pressed
 *
 * @param[in] x - the x location of the mouse when a key was pressed
 *
 * @param[in] y - the y location of the mouse when a key was pressed
 *
 *****************************************************************************/
void keyboard(unsigned char key, int x, int y)
{
   //create a point object for the event
   Point currentLoc(x, y);

   universalEventProcessor(new KeyboardEvent(key, currentLoc));
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * Function for the mouseClick callback
 *
 * @param[in] button - the button that was pressed
 *
 * @param[in] state - the state of the button
 *
 * @param[in] x - the x location of the mouse when a key was pressed
 *
 * @param[in] y - the y location of the mouse when a key was pressed
 *
 *****************************************************************************/
void mouseClick(int button, int state, int x, int y)
{
   //create a point object for the event
   Point currentLoc(x, y);

   universalEventProcessor(new MouseClickEvent(button, state, currentLoc));
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * Function for the reshape callback
 *
 * @param[in] w - the width of the screen
 *
 * @param[in] h - the height of the screen
 *
 *****************************************************************************/
void reshape(const int w, const int h)
{
   universalEventProcessor(new ReshapeEvent(w, h));
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * Function for the close window callback
 *
 *****************************************************************************/
void onClose()
{
   tracker closeAction;

   //delete all user shapes
   closeAction.deleteAllShapes();

   //delete everything in the menu
   closeAction.deleteMenu();

   //close window
   glutLeaveMainLoop();
}

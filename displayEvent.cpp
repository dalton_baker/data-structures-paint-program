/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the display event class
 *****************************************************************************/
#include "displayEvent.h"


/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the display event processor
 *
 *****************************************************************************/
void DisplayEvent::processEvent()
{
   tracker displayAction;

   //redraw the menu for the current screen height
   displayAction.resizeMenu();

   //redraw all the shapes to the screen
   displayAction.drawShapes();
}

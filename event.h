/**************************************************************************//**
 * @file
 * @brief Header file to be used with Event.cpp
 *****************************************************************************/
#ifndef __EVENT__H__
#define __EVENT__H__

#include <GL/freeglut.h>
#include "tracker.h"
#include <cctype>

/*!
* @brief A base class for all events that can take place
*/
class Event
{
public:
   /*! A virtual destructior for all events */
   virtual ~Event();
   /*! virtuall function to process any event */
   virtual void processEvent() = 0;
};

#endif

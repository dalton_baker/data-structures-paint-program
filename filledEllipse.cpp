/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the filled ellipse class
 *****************************************************************************/
#include "filledEllipse.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the filled ellipse constructor
 *
 * @param[in] p - point where the filled ellipse will be drawn from
 *
 * @param[in] h - the height of the filled ellipse
 *
 * @param[in] w - the width of the filled ellipse
 *
 * @param[in] color - the index of the color used for the filled ellipse
 *
 * @param[in] fcolor - the index of the fill color used for the filled ellipse
 *
 *****************************************************************************/
FilledEllipse::FilledEllipse(Point p, float h, float w, int color, int fcolor )
              : Shape(p, h, w, color)
{
   fillColor = fcolor;
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * This is will draw a filled ellipse to the screen.
 * Most of this code was provide, but I did tweak it a lot. I still don't
 * really understand how it works.
 *
 *****************************************************************************/
void FilledEllipse::draw()
{
   float radius = abs(width) < abs(height) ? abs(width)/2 : abs(height)/2;

   //this first part draws the inner filled ellipse
   glColor3fv( colors[fillColor] );
   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();
   glTranslatef( location.returnX() + (width/2), glutGet(GLUT_WINDOW_HEIGHT) -
                 location.returnY() - (height/2), 0 );

	// by ratio of major to minor axes
   glScalef( (width/2) / radius, (height/2) / radius, 1.0 );
   GLUquadricObj *disk = gluNewQuadric();
   gluDisk( disk, 0, radius, int( radius ), 1 );
   gluDeleteQuadric( disk );
   glLoadIdentity();
   glFlush();

   //this part draws a normal emply ellipse on top of the filled one
   glColor3fv( colors[shapeColor] );
   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();
   glTranslatef( location.returnX() + (width/2), glutGet(GLUT_WINDOW_HEIGHT) -
                 location.returnY() - (height/2), 0 );

   // by ratio of major to minor axes
   glScalef( (width/2) / radius, (height/2) / radius, 1.0 );
   GLUquadricObj *circle = gluNewQuadric();
   gluDisk( circle, radius - 2, radius, int( radius ), 1 );
   gluDeleteQuadric( circle );
   glLoadIdentity();
   glFlush();

}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will tell you if a filled ellipse contains a point
 *
 * @param[in] p - point to check the location of
 *
 * @return - a bool telling you if the point is in the shape
 *
 *****************************************************************************/
bool FilledEllipse::contains(Point p)
{
   //get the center of the ellipse (h,k)
   float h = location.returnX() + (width/2);
   float k = location.returnY() + (height/2);

   //if this is less than one then we are inside the ellipse
   float tester = (pow((p.returnX()-h), 2)/pow(width/2, 2)) +
                  (pow((p.returnY()-k), 2)/pow(height/2, 2));

   //check if tester was less than one and return true if it was
   if( tester <= 1)
   {
      return true;
   }

   //return false if the point was outside the ellipse
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the fill color of a filled ellipse
 *
 * @return - the index to the fill color
 *
 *****************************************************************************/
int FilledEllipse::getFillColor()
{
   return fillColor;
}

/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the shape base class
 *****************************************************************************/
#include "shape.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the shape constructor
 *
 * @param[in] p - point where the shape will be drawn from
 *
 * @param[in] h - the height of the shape
 *
 * @param[in] w - the width of the shape
 *
 * @param[in] color - the index of the color used for the shape
 *
 *****************************************************************************/
Shape::Shape(Point p, float h, float w, int color)
{
   location = p;
   height = h;
   width = w;
   shapeColor = color;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the shape deconstructor
 *
 *****************************************************************************/
Shape::~Shape()
{
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will give you the coordinates of a shape
 *
 * @return - the point that the shape was drawn from
 *
 *****************************************************************************/
Point Shape::getCoords()
{
   return location;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will give you the height of a shape
 *
 * @return - the height of the shape
 *
 *****************************************************************************/
float Shape::getHeight()
{
   return height;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will give you the width of a shape
 *
 * @return - the width of the shape
 *
 *****************************************************************************/
float Shape::getWidth()
{
   return width;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will give you the color of a shape
 *
 * @return - the color of the shape
 *
 *****************************************************************************/
int Shape::getColor()
{
   return shapeColor;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will let you resize a shape
 *
 * @param[in] newH - the new height of the shape
 *
 * @param[in] newW - the new width of the shape
 *
 *****************************************************************************/
void Shape::resize(float newH, float newW)
{
   height = newH;
   width = newW;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will let you move a shape
 *
 * @param[in] p - the new location to draw the shape from
 *
 *****************************************************************************/
void Shape::move(Point p)
{
   location = p;
}

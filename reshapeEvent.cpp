/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the ellipse class
 *****************************************************************************/
#include "reshapeEvent.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the reshape event constructor
 *
 * @param[in] width - the width of the screen
 *
 * @param[in] height - the height of the screen
 *
 *****************************************************************************/
ReshapeEvent::ReshapeEvent(int width, int height)
{
   screenHeight = height;
   screenWidth = width;
}

/**************************************************************************//**
 * @author Dr. Hinker
 *
 * @par Description:
 * This is the display reshape event processor
 *
 *****************************************************************************/
void ReshapeEvent::processEvent()
{
   // use an orthographic projection
   glMatrixMode( GL_PROJECTION );
   // initialize transformation matrix
   glLoadIdentity();
   // make OpenGL coordinates
   gluOrtho2D( 0.0, screenWidth, 0.0, screenHeight );
   // the same as the screen coordinates
   glViewport( 0, 0, screenWidth, screenHeight );
}

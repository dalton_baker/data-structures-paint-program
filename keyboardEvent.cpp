/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the keyboard event class
 *****************************************************************************/
#include "keyboardEvent.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the keyboard event constructor
 *
 * @param[in] k - the key that was pressed
 *
 * @param[in] p - the location of the click
 *
 *****************************************************************************/
KeyboardEvent::KeyboardEvent(int k, Point p)
{
   key = k;
   location = p;
}

/**************************************************************************//**
 * @author Dalton Baker & Rob Minick
 *
 * @par Description:
 * This is the keyboard event processor
 *
 *****************************************************************************/
void KeyboardEvent::processEvent()
{
   tracker keyboardAction;

   //escape key & q closes window
   if(key == ESC_KEY || key == Q_KEY)
   {
      //delete all user shapes
      keyboardAction.deleteAllShapes();

      //delete everything in the menu
      keyboardAction.deleteMenu();

      //close window
      glutLeaveMainLoop();
   }

   //c key deletes all shapes made by the user
   else if(key == C_KEY)
   {
      //clear the screen
      keyboardAction.deleteAllShapes();
   }

   //d key removes the top shape made by the user
   else if (key == D_KEY)
   {
       keyboardAction.removeLastShape();
   }

   //redraw the screen after any action
   keyboardAction.drawShapes();
}

/**************************************************************************//**
 * @file
 * @brief Header file to be used with keyboardEvent.cpp
 *****************************************************************************/
#ifndef __KEYBOARDEVENT__H__
#define __KEYBOARDEVENT__H__

#include "event.h"

/*!
* @brief Defining the escape key
*/
#define ESC_KEY 27
/*!
* @brief Defining the delete key
*/
#define DELETE_KEY 8
/*!
* @brief Defining the 'd' key
*/
#define D_KEY 100
/*!
* @brief Defining the 'q' key
*/
#define Q_KEY 113
/*!
* @brief Defining the 'c' key
*/
#define C_KEY 99

/*!
* @brief A class for when the keyboard is used
*/
class KeyboardEvent : public Event
{
protected:
   /*! The key that was pressed */
   unsigned int key;
   /*! the location of the mouse durring the key press */
   Point location;
public:
   KeyboardEvent(int k, Point p);
   void processEvent();
};

#endif

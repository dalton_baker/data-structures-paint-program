/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the ellipse class
 *****************************************************************************/
#include "ellipse.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the ellipse constructor
 *
 * @param[in] p - point where the ellipse will be drawn from
 *
 * @param[in] h - the height of the ellipse (the y diameter)
 *
 * @param[in] w - the width of the ellipse (the x diameter)
 *
 * @param[in] color - the index of the color used for the ellipse
 *
 *****************************************************************************/
Ellipse::Ellipse(Point p, float h, float w, int color ) : Shape(p, h, w, color)
{
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * This is will draw a ellipse to the screen
 * Most of this code was provide, but I did tweak it a lot
 *
 *****************************************************************************/
void Ellipse::draw()
{
   float radius = abs(width) < abs(height) ? abs(width)/2 : abs(height)/2;
   glColor3fv( colors[shapeColor] );
   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();
   glTranslatef( location.returnX()+(width/2), glutGet(GLUT_WINDOW_HEIGHT)
                 -location.returnY()-(height/2), 0 );

   // by ratio of major to minor axes
   glScalef( (width/2) / radius, (height/2) / radius, 1.0 );
   GLUquadricObj *disk = gluNewQuadric();
   gluDisk( disk, radius - 2, radius, int( radius ), 1 );
   gluDeleteQuadric( disk );
   glLoadIdentity();
   glFlush();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will tell you if a ellipse contains a point. I wrote all this code, but
 * I just looked up the eqasion for finding a point inside an ellipse. I didn't
 * work out the math, so I don't really know why this works
 *
 * @param[in] p - point to check the location of
 *
 * @return - a bool telling you if the point is in the shape
 *
 *****************************************************************************/
bool Ellipse::contains(Point p)
{
   //get the center of the ellipse (h,k)
   float h = location.returnX() + (width/2);
   float k = location.returnY() + (height/2);

   //if this is less than one then we are inside the ellipse
   float tester = (pow((p.returnX()-h), 2)/pow(width/2, 2)) +
                  (pow((p.returnY()-k), 2)/pow(height/2, 2));

   //check if tester was less than one and return true if it was
   if( tester <= 1)
   {
      return true;
   }

   //return false if the point was outside the ellipse
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return a -1, indicating that this is not a filled object
 *
 * @return - -1
 *
 *****************************************************************************/
int Ellipse::getFillColor()
{
   return -1;
}

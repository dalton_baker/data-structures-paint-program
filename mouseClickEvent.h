/**************************************************************************//**
 * @file
 * @brief Header file to be used with mouseClickEvent.cpp
 *****************************************************************************/
#ifndef __MOUSECLICKEVENT__H__
#define __MOUSECLICKEVENT__H__

#include "event.h"

/*!
* @brief A class for when the mouse is clicked
*/
class MouseClickEvent : public Event
{
   /*! the mouse button that was clicked */
   int button;
   /*! the state of the click */
   int state;
   /*! the location of the mouse durring the button press */
   Point location;
public:
   MouseClickEvent(int but, int st, Point p);
   void processEvent();
};


#endif

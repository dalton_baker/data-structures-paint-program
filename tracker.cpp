/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the tracker class
 *****************************************************************************/
#include "tracker.h"

std::vector<Shape *> tracker::shapesOnScreen;
std::vector<Shape *> tracker::menu;

//new shape variables
Point tracker::whereToDraw;
bool tracker::currentlyDrawing = false;
int tracker::newShapeColor = 4;
int tracker::newFillColor = 5;
int tracker::newShapeType = none;

//move shape variables
Shape *tracker::shape2move;
bool tracker::currentlyMoving = false;
Point tracker::initialPos;

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the tracker constructor
 *
 *****************************************************************************/
tracker::tracker()
{
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the event deconstructor
 *
 *****************************************************************************/
tracker::~tracker()
{
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will add a shape given to it to the vector containing all the shapes
 * drawn by the user
 *
 * @param[in] *newShape - a pointer to the new shape
 *
 *****************************************************************************/
void tracker::addShape(Shape *newShape)
{
   shapesOnScreen.push_back(newShape);
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will create the menu, it is built from scratch every time
 *
 *****************************************************************************/
void tracker::createMenu()
{
   //the side length of each square in the menu
   float sLen = glutGet(GLUT_WINDOW_HEIGHT) / 13;

   //the percentage that will be used for drawing the smaller shapes in the
   //shape selctor part of the menu
   float percentage = 0.15;

   //this will be used to calculate where each menu item will be drawn
   Point loc;

   //cycle through and draw the left hand side of the menu
   for(int i = 0; i < 13; i++)
   {
      loc.updateY( i*sLen+i );
      menu.push_back(new FilledRectangle(loc, sLen, sLen, 3, i>2 ? i+1 : 1));
   }

   //cycle through and draw the right hand side of the menu
   loc.updateX(sLen+1);
   for(int i = 0; i < 13; i++)
   {
      loc.updateY( i*sLen+i );
      menu.push_back(new FilledRectangle(loc, sLen, sLen, 3, i>2 ? i+11 : 1));
   }

   //calculate the side of each smaller display shape for the shape selector
   float dispSide = sLen*(1.0-percentage*2.0);

   //create a point to keep track of where the display shapes will be drawn
   loc.updateX(sLen*percentage);
   loc.updateY(sLen*percentage);
   menu.push_back(new Line(loc, dispSide, dispSide, 0));

   loc.updateY(sLen*(1.0+percentage)+1);
   menu.push_back(new Ellipse(loc, dispSide, dispSide, 0));

   loc.updateY(sLen*(2.0+percentage)+2);
   menu.push_back(new Rectangle(loc, dispSide, dispSide, 0));

   loc.updateX(sLen*(1.0+percentage));
   menu.push_back(new FilledRectangle(loc, dispSide, dispSide, 0, 2));

   loc.updateY(sLen*(1.0+percentage)+1);
   menu.push_back(new FilledEllipse(loc, dispSide, dispSide, 0, 2));

   //this will slect the shape to be draw in the user selected display
   loc.updateY(sLen*percentage);
   switch (newShapeType)
   {
      case none:
         break;

      case line:
         menu.push_back(new Line(loc, dispSide, dispSide, newShapeColor));
         break;

      case rectangle:
         menu.push_back(new Rectangle(loc, dispSide, dispSide, newShapeColor));
         break;

      case ellipse:
         menu.push_back(new Ellipse(loc, dispSide, dispSide, newShapeColor));
         break;

      case filledRectangle:
         menu.push_back(new FilledRectangle(loc, dispSide, dispSide,
                        newShapeColor, newFillColor));
         break;

      case filledEllipse:
         menu.push_back(new FilledEllipse(loc, dispSide, dispSide,
                        newShapeColor, newFillColor));
         break;
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This deletes and then redraws the menu
 *
 *****************************************************************************/
void tracker::resizeMenu()
{
   //delete the menu
   tracker::deleteMenu();

   //recreate the menu
   tracker::createMenu();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This Draws all of the items in the menu and all the items created by the
 * user to the screen
 *
 *****************************************************************************/
void tracker::drawShapes()
{
   glClear(GL_COLOR_BUFFER_BIT);

   //draw all the shapes the user made
   for (Shape *shape : shapesOnScreen)
   {
      shape->draw();
   }

   //draw all the shapes on the menu
   for (Shape *shape : menu)
   {
      shape->draw();
   }

   glutSwapBuffers();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This deletes all the shapes made by the user
 *
 *****************************************************************************/
void tracker::deleteAllShapes()
{
   //delete all pointer contents
   for (Shape *shape : shapesOnScreen)
   {
      delete shape;
   }

   //delete all the pointers
   shapesOnScreen.clear();
}

/**************************************************************************//**
 * @author Dalton Baker & Rob Minick
 *
 * @par Description:
 * This will search through all the shapes drawn by the user and find if there
 * is one that contains the provided point
 *
 * @param[in] p - point to check the location of
 *
 * @return - the pointer to the shape containing the point
 *
 *****************************************************************************/
Shape *tracker::screenContShape(Point p)
{

   std::vector<Shape*> revshapesOnScreen;

   //reverse the vector, so we can find the top shape
   for (Shape *shape : shapesOnScreen)
   {
       revshapesOnScreen.insert(revshapesOnScreen.begin(),shape);
   }

   //search through each shape to find one that contains the point
   for (Shape* shape : revshapesOnScreen)
   {
      if(shape->contains(p))
         return shape;
   }

   //return nullptr if no shape contains the point
   return nullptr;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This deletes all the shapes in the menu
 *
 *****************************************************************************/
void tracker::deleteMenu()
{
      //delete all pointer contents
      for (Shape *shape : menu)
      {
         delete shape;
      }

      //delete all the pointers
      menu.clear();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will search through all the shape in the menu and find if there
 * is one that contains the provided point
 *
 * @param[in] p - point to check the location of
 *
 * @return - the pointer to the shape containing the point
 *
 *****************************************************************************/
Shape *tracker::menuContShape(Point p)
{
   //seach through each item in the menu to find if one was clicked
   for (Shape *shape : menu)
   {
      if(shape->contains(p))
         return shape;
   }

   //return nullptr if one wasn't clicked
   return nullptr;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This returns the type of shape selected from the menu
 *
 * @param[in] s - the shape we are searching
 *
 * @return - the type of shape
 *
 *****************************************************************************/
int tracker::getMenuSelection(Shape *s)
{
   Point location = s->getCoords();

   //if the click was in the first column
   if(location.returnX() == 0 )
   {
      if(location.returnY() == 0)
         return line;
      else if(location.returnY() < glutGet(GLUT_WINDOW_HEIGHT)/13*2)
         return ellipse;
      else
         return rectangle;
   }

   //if the click was in the second column
   else
   {
      if(location.returnY() == 0)
         return none;
      else if(location.returnY() < glutGet(GLUT_WINDOW_HEIGHT)/13*2)
         return filledEllipse;
      else
         return filledRectangle;
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This sets the color of the new shape to be drawn by the user
 *
 * @param[in] color - the index of the color
 *
 *****************************************************************************/
void tracker::setNewColor(int color)
{
   //set the color the user selected
   newShapeColor = color;

   //redraw the menu
   tracker::resizeMenu();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This sets the fill color of the new shape to be drawn by the user
 *
 * @param[in] color - the index of the fill color
 *
 *****************************************************************************/
void tracker::setFillColor(int color)
{
   //set the fill color the user selceted
   newFillColor = color;

   //redraw the menu
   tracker::resizeMenu();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This sets the type of new shape to be drawn by the user
 *
 * @param[in] shape - the type of shape
 *
 *****************************************************************************/
void tracker::setNewShapeType(int shape)
{
   //set the shape type the user selected
   newShapeType = shape;

   //redraw the menu
   tracker::resizeMenu();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This sets the new shape location
 *
 * @param[in] p - the point where the new shape will be drawn from
 *
 *****************************************************************************/
void tracker::beginDraw(Point p)
{
   //store the stating point
   whereToDraw = p;

   //set the drawing flag
   currentlyDrawing = true;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will determine if the new shapes ending location is valid, and then
 * add it to the vector of shapes made by the user
 *
 * @param[in] p - the ending point of the new shape to be drawn
 *
 *****************************************************************************/
void tracker::endDraw(Point p)
{
   //reset drawing flag
   currentlyDrawing = false;

   //mkae sure the new shape is a valid size
   if(whereToDraw == p)
   {
      return;
   }

   //the diference between the starting and ending point will be the dimentions
   Point dimentions = p - whereToDraw;

   //figure out what type of shape to draw
   switch (newShapeType)
   {
      case none:
         return;

      case line:
         tracker::addShape(new Line(whereToDraw, dimentions.returnY(),
                           dimentions.returnX(), newShapeColor));
         break;

      case rectangle:
         tracker::addShape(new Rectangle(whereToDraw, dimentions.returnY(),
                           dimentions.returnX(), newShapeColor));
         break;

      case ellipse:
         tracker::addShape(new Ellipse(whereToDraw, dimentions.returnY(),
                           dimentions.returnX(), newShapeColor));
         break;

      case filledRectangle:
         tracker::addShape(new FilledRectangle(whereToDraw, dimentions.returnY(),
                           dimentions.returnX(), newShapeColor, newFillColor));
         break;

      case filledEllipse:
         tracker::addShape(new FilledEllipse(whereToDraw, dimentions.returnY(),
                           dimentions.returnX(), newShapeColor, newFillColor));
         break;
   }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This returns a bool indicating wether a shape is currently being drawn
 * by the user
 *
 * @return - true if the user is currently drawing a shap, false if not
 *
 *****************************************************************************/
bool tracker::returnCurrDraw()
{
   return currentlyDrawing;
}


/**************************************************************************//**
 * @author Rob Minick
 *
 * @par Description:
 * This sets all the variables up to begin a move
 *
 * @param[in] s - The shape that will be moved
 *
 * @param[in] p - the initial point of the click
 *
 *****************************************************************************/
void tracker::beginMove(Shape *s, Point p)
{

  shape2move = s;

  int count = 0;
  for(Shape *shape : shapesOnScreen)
  {
     if(shape == shape2move)
        shapesOnScreen.erase(shapesOnScreen.begin() + count);

     count++;
  }

  addShape(shape2move);
  drawShapes();
  initialPos = p;
  currentlyMoving = true;
}

/**************************************************************************//**
 * @author Rob Minick
 *
 * @par Description:
 * This decides if a move will take place and then moves the shape to the
 * ending position
 *
 * @param[in] p - the ending point of the click
 *
 *****************************************************************************/
void tracker::endMove(Point p)
{
   currentlyMoving = false;

   if(initialPos == p)
   {
      return;
   }

  Point temp(shape2move->getWidth()/2, shape2move->getHeight()/2);

  temp = p - temp;

  shape2move->move(temp);
}

/**************************************************************************//**
 * @author Rob Minick & Dalton Baker
 *
 * @par Description:
 * This removes the top object on a
 *
 *****************************************************************************/
void tracker::removeLastShape()
{
  if(!shapesOnScreen.empty())
  {
      //delete pointer conents
      delete shapesOnScreen.back();
      //remove the pointer from the vector
      shapesOnScreen.pop_back();
  }
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This sets flag variables to zero in case the user ends a move on the menu
 *
 *****************************************************************************/
void tracker::clearFlags()
{
   currentlyMoving = false;
   currentlyDrawing = false;
}

/**************************************************************************//**
 * @author Rob Minick & Dalton Baker
 *
 * @par Description:
 * This tells you if a move is currently happening
 *
 * @return - true value of currentlyMoving
 *
 *****************************************************************************/
bool tracker::returnCurrMove()
{
  return currentlyMoving;
}

/**************************************************************************//**
 * @file
 * @brief Header file to be used with line.cpp
 *****************************************************************************/
#ifndef __LINE__H__
#define __LINE__H__

#include "shape.h"

/*!
* @brief A class for the line shape
*/
class Line : public Shape
{
public:
   Line(Point p, float h, float w, int color);
   void draw();
   bool contains(Point p);
   int getFillColor();
};


#endif

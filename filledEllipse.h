/**************************************************************************//**
 * @file
 * @brief Header file to be used with filledEllipse.cpp
 *****************************************************************************/
#ifndef __FILLEDELLIPSE__H__
#define __FILLEDELLIPSE__H__

#include "shape.h"

/*!
* @brief A class for the filled ellipse shape
*/
class FilledEllipse : public Shape
{
protected:
   /*! The index for the fill color if the ellipse */
   int fillColor;
public:
   FilledEllipse(Point p, float h, float w, int color, int fcolor);
   void draw();
   bool contains(Point p);
   int getFillColor();
};

#endif

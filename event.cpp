/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the event base class
 *****************************************************************************/
#include "event.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the event deconstructor, This only exists so we can delete
 * the pointer
 *
 *****************************************************************************/
Event::~Event()
{
}

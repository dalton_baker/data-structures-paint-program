/**************************************************************************//**
 * @file
 * @brief Header file to be used with tracker.cpp
 *****************************************************************************/
#ifndef __TRACKER__H__
#define __TRACKER__H__

#include "line.h"
#include "rectangle.h"
#include "ellipse.h"
#include "filledRectangle.h"
#include "filledEllipse.h"
#include <iostream>
#include <vector>

/*!
* @brief This is for assigning the shape types numbers
*/
enum{
   none,
   line,
   rectangle,
   ellipse,
   filledRectangle,
   filledEllipse
};

/*!
* @brief This class keeps track of every shape that exists
*/
class tracker
{
protected:
   /*! A vector that holds the pointers to all the shapes made by the user */
   static std::vector<Shape *> shapesOnScreen;
   /*! A vector that holds the pointers to all the shapes made for the menu */
   static std::vector<Shape *> menu;

   //new shape vars
   /*! This is the point where a new shape will be drawn */
   static Point whereToDraw;
   /*! This tells the mouse click function if a drawing event is taking place */
   static bool currentlyDrawing;
   /*! This is the color of the new shape that will be drawn */
   static int newShapeColor;
   /*! This is the fill color of the new shape that will be drawn */
   static int newFillColor;
   /*! This is the type of the new shape that will be drawn */
   static int newShapeType;

   //shape add vars
   /*! This is the shape that will be moved if a move takes place */
  static Shape *shape2move;
  /*! This tells the mouse click function if a moving event is taking place */
  static bool currentlyMoving;
  /*! This is the starting position of the mouse when a move happens */
  static Point initialPos;

public:
   tracker();
   ~tracker();

   void addShape(Shape *newShape);
   void drawShapes();
   void deleteAllShapes();
   void createMenu();
   void resizeMenu();
   void deleteMenu();
   Shape *screenContShape(Point p);
   Shape *menuContShape(Point p);
   int getMenuSelection(Shape *s);
   void removeLastShape();
   void clearFlags();

   //functions for menu selection
   void setNewColor(int color);
   void setFillColor(int color);
   void setNewShapeType(int shape);

   //functions for drawing a shape
   void beginDraw(Point p);
   void endDraw(Point p);
   bool returnCurrDraw();

   //functions for moving shapes
   void beginMove(Shape *s, Point p);
   void endMove(Point p);
   bool returnCurrMove();
};

#endif

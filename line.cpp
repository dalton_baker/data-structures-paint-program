/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the line class
 *****************************************************************************/
#include "line.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the line constructor
 *
 * @param[in] p - point where the line will be drawn from
 *
 * @param[in] h - the height of the line
 *
 * @param[in] w - the width of the line
 *
 * @param[in] color - the index of the color used for the line
 *
 *****************************************************************************/
Line::Line(Point p, float h, float w, int color) : Shape(p, h, w, color)
{
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * This is will draw a line to the screen
 * Most of this code was provide, but I did tweak it a lot
 *
 *****************************************************************************/
void Line::draw()
{
   float x1 = location.returnX();
   float x2 = location.returnX() + width;
   float y1 = glutGet(GLUT_WINDOW_HEIGHT) - location.returnY();
   float y2 = glutGet(GLUT_WINDOW_HEIGHT) - location.returnY() - height;

   glLineWidth( 2 );
   glColor3fv( colors[shapeColor] );
    glBegin( GL_LINES );
        glVertex2f( x1, y1 );
        glVertex2f( x2, y2 );
    glEnd();
    glFlush();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will tell you if a line contains a point
 *
 * @param[in] p - point to check the location of
 *
 * @return - a bool telling you if the point is in the shape
 *
 *****************************************************************************/
bool Line::contains(Point p)
{
   //how close do you want to be to the line
   float buffer = 5;

   //find the number of points we will need to create to cover the line
   Point endPoint(width, height);
   endPoint = endPoint + location;
   int numOfPoints = location.distBetween(endPoint) / (buffer*2);

   //used to find the location of points on the line to check
   float yStep = height/numOfPoints;
   float xStep = width/numOfPoints;

   //cycle through points on the line that are
   for( int i = 0; i <= numOfPoints; i++ )
   {
      //create the point
      Point check(xStep * i, yStep * i);
      check = check + location;

      //check the distance of the point from the mouse click
      if(check.distBetween(p) < buffer)
      {
         return true;
      }
   }

   //return false if the shape is not in the area of the click
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return a -1, indicating that this is not a filled object
 *
 * @return - -1
 *
 *****************************************************************************/
int Line::getFillColor()
{
   return -1;
}

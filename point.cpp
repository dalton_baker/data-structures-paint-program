/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the point class
 *****************************************************************************/
#include "point.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the default Point constructor, point initialized to (0,0)
 *
 *****************************************************************************/
Point::Point()
{
   x = 0;
   y = 0;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is a Point constructor
 *
 * @param[in] xCoord - the x coordinate of the point
 *
 * @param[in] yCoord - the y coordinate of the point
 *
 *****************************************************************************/
Point::Point(float xCoord, float yCoord)
{
   x = xCoord;
   y = yCoord;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will give you the x value of the Point
 *
 * @return - the x value of the Point
 *
 *****************************************************************************/
float Point::returnX()
{
   return x;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will give you the y value of the Point
 *
 * @return - the y value of the Point
 *
 *****************************************************************************/
float Point::returnY()
{
   return y;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will let you change the x value of the Point
 *
 * @param[in] newX - the new x coordinate of the point
 *
 *****************************************************************************/
void Point::updateX(float newX)
{
   x = newX;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will let you change the y value of the Point
 *
 * @param[in] newY - the new y coordinate of the point
 *
 *****************************************************************************/
void Point::updateY(float newY)
{
   y = newY;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is will return the distance between our point and a point given
 *
 * @param[in] p - The point to check out disptance from
 *
 * @return - the ditance from the other point
 *
 *****************************************************************************/
float Point::distBetween(Point p)
{
   //create a new point with the difference between our x and y values
   Point temp(x-p.x, y-p.y);

   //use the pythagorean theorem on the difference
   return sqrt( pow(temp.returnX(),2) + pow(temp.returnY(),2) );
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an operator overloader for "+" for Points
 *
 * @param[in] p - the Point to the right of the operator
 *
 * @return - the result of the addition
 *
 *****************************************************************************/
Point Point::operator+(const Point &p)
{
   //create a point that is the 2 points added together
   Point result(this->x + p.x, this->y + p.y);

   //return it
   return result;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an operator overloader for "-" for Points
 *
 * @param[in] p - the Point to the right of the operator
 *
 * @return - the result of the subtaction
 *
 *****************************************************************************/
Point Point::operator-(const Point &p)
{
   //create a point that is the difference between the 2 points
   Point result(this->x - p.x, this->y - p.y);

   //return it
   return result;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an operator overloader for "=" for Points
 *
 * @param[in] p - the Point to the right of the operator
 *
 * @return - the pointer to the operand on the right side
 *
 *****************************************************************************/
Point Point::operator=(const Point &p)
{
   //set the point on the lhs to the point on the rhs
   x = p.x;
   y = p.y;

   //return it
   return *this;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an operator overloader for "==" for Points
 *
 * @param[in] p - the Point to the right of the operator
 *
 * @return - the pointer to the operand on the right side
 *
 *****************************************************************************/
bool Point::operator==(const Point &p)
{
   //chack if x is the same
   if( x == p.x )
   {
      //check if y is the same
      if( y == p.y)
      {
         //return true if both are the same
         return true;
      }
   }

   //return false if one or both of them do not match
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is an operator overloader to print points, used for debugging
 *
 * @param[in] p - the Point to the left of the operator
 *
 * @param[in] out - the ostream we are printing to
 *
 * @return - the ostream
 *
 *****************************************************************************/
std::ostream &operator<<(std::ostream &out, Point &p)
{
   //print the x and y values to out
   out << "(" << p.x << ", " << p.y << ")";
   return out;
}

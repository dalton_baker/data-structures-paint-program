/**************************************************************************//**
 * @file
 * @brief Header file to be used with reshapeEvent.cpp
 *****************************************************************************/
#ifndef __RESHAPEEVENT__H__
#define __RESHAPEEVENT__H__

#include "event.h"

/*!
* @brief A class for when the screen is resized
*/
 class ReshapeEvent : public Event
{
protected:
   /*! A float to hold the screen height */
   float screenHeight;
   /*! A float to hold the screen width */
   float screenWidth;
public:
   ReshapeEvent(int height, int width);
   void processEvent();
};

#endif

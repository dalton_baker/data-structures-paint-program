/**************************************************************************//**
 * @file
 * @brief Header file to be used with point.cpp
 *****************************************************************************/
#ifndef __POINT__H__
#define __POINT__H__

#include <iostream>
#include <cmath>

/*!
* @brief A class for a point on the screen
*/
class Point
{
protected:
   /*! the x position of the point */
   float x;
   /*! the y position of the point */
   float y;
public:
   Point();
   Point(float xCoord, float yCoord);
   float returnX();
   float returnY();
   void updateX(float newX);
   void updateY(float newY);
   float distBetween(Point p);

   Point operator+(const Point &p);
   Point operator-(const Point &p);
   Point operator=(const Point &p);
   bool operator==(const Point &p);

   friend std::ostream &operator<<(std::ostream &out, Point &p);
};

#endif

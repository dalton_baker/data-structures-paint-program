/**************************************************************************//**
 * @file
 * @brief This is the header for callbacks.cpp,
 * This code was provided by Dr. Hinker
 *****************************************************************************/
#ifndef __CALLBACKS__H__
#define __CALLBACKS__H__

#include "displayEvent.h"
#include "keyboardEvent.h"
#include "mouseClickEvent.h"
#include "reshapeEvent.h"


//declaration for the universal processor function
void universalEventProcessor(Event *Event);
// Callback for the display Event
void display();
// Callback for the keyboard Event
void keyboard(unsigned char key, int x, int y);
// Callback for the mouseClick Event
void mouseClick(int button, int state, int x, int y);
// Callback for the reshape Event
void reshape(const int x, const int y);
//calback for screen close
void onClose();

#endif

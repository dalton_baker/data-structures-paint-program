/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the mouse click event class
 *****************************************************************************/
#include "mouseClickEvent.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the mouse click event constructor
 *
 * @param[in] but - the button that was clicked
 *
 * @param[in] st - the state of the button
 *
 * @param[in] p - the location of the click
 *
 *****************************************************************************/
MouseClickEvent::MouseClickEvent(int but, int st, Point p)
{
   button = but;
   state = st;
   location = p;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the mouse click event processor
 *
 *****************************************************************************/
void MouseClickEvent::processEvent()
{
   tracker mouseAction;

   //this tells us if we are inside the menu
   bool insideMenu = location.returnX() < glutGet(GLUT_WINDOW_HEIGHT)/13*2;

   //this tells us if we are in the menu shape selection
   bool menuShapeSel = location.returnY() < glutGet(GLUT_WINDOW_HEIGHT)/13*3;

   //search to see if the user clicked an item in the menu
   Shape *shapeClicked = mouseAction.menuContShape(location);

   //if we are clicking inside the menu
   if(insideMenu)
   {
      //this will get the type of shape selected by the user
      if(menuShapeSel && button == 0 && state == 0 && shapeClicked)
      {
         int newShapeType = mouseAction.getMenuSelection(shapeClicked);
         mouseAction.setNewShapeType(newShapeType);
      }

      // this will get the color selected by the user
      else if(!menuShapeSel && shapeClicked)
      {
         //this is if the color selected id the outline color
         if(button == 0 && state == 0)
         {
            mouseAction.setNewColor(shapeClicked->getFillColor());
         }

         //this is if the color selected was a fill color
         else if(button == 2 && state == 0)
         {
            mouseAction.setFillColor(shapeClicked->getFillColor());
         }
      }

      //this clears the flags for moving shapes and drawing shapes
      mouseAction.clearFlags();
   }

   //if we are clicking outside the menu
   else
   {
      //this is for when the user begins drawing a shape
      if(button == 0 && state == 0)
      {
         mouseAction.beginDraw(location);
      }

      //this is for when a user finishes drawing a shape
      else if(button == 0 && state == 1 && mouseAction.returnCurrDraw())
      {
         mouseAction.endDraw(location);
      }

      //search to see if we clicked a shape made by the user
      shapeClicked = mouseAction.screenContShape(location);

      //this begins moving the shape if the user right clicker on it
      if(button == 2 && state == 0 && shapeClicked)
      {
         mouseAction.beginMove(shapeClicked, location);
      }

      //this moves the shape to a new location if it is valid
      else if(button == 2 && state == 1 && mouseAction.returnCurrMove())
      {
         mouseAction.endMove(location);
      }
   }

   //redraw the screen when done
   mouseAction.drawShapes();
}

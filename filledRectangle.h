/**************************************************************************//**
 * @file
 * @brief Header file to be used with filledRectangle.cpp
 *****************************************************************************/
#ifndef __FILLEDRECTANGLE__H__
#define __FILLEDRECTANGLE__H__

#include "shape.h"

/*!
* @brief A class for the filled rectangle shape
*/
class FilledRectangle : public Shape
{
protected:
   /*! The index for the fill color if the rectangle */
   int fillColor;
public:
   FilledRectangle(Point p, float h, float w, int color, int fcolor);
   void draw();
   bool contains(Point p);
   int getFillColor();
};

#endif

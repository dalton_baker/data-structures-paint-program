/**************************************************************************//**
 * @file
 * @brief This file holds all the members of the filled rectangle class
 *****************************************************************************/
#include "filledRectangle.h"

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This is the filled rectangle constructor
 *
 * @param[in] p - point where the filled rectangle will be drawn from
 *
 * @param[in] h - the height of the filled rectangle
 *
 * @param[in] w - the width of the filled rectangle
 *
 * @param[in] color - the index of the color used for the filled rectangle
 *
 * @param[in] fcolor - the index of the fill color used for the rectangle
 *
 *****************************************************************************/
FilledRectangle::FilledRectangle(Point p, float h, float w,
      int color, int fcolor) : Shape(p, h, w, color)
{
   fillColor = fcolor;
}

/**************************************************************************//**
 * @author Dalton Baker / Dr. Hinker Provided
 *
 * @par Description:
 * This is will draw a filled rectangle to the screen
 * Most of this code was provide, but I did tweak it a lot
 *
 *****************************************************************************/
void FilledRectangle::draw()
{
   //first we need to calculate x1, x2, y1, & y2 using our stored values
   int x1 = location.returnX();
   int x2 = location.returnX() + width;
   int y1 = glutGet(GLUT_WINDOW_HEIGHT)- location.returnY();
   int y2 = glutGet(GLUT_WINDOW_HEIGHT)- location.returnY() - height;

   //Draw inside
      glColor3fv( colors[fillColor] );
   glBegin( GL_POLYGON );
      glVertex2f( x1, y1 );
      glVertex2f( x2, y1 );
      glVertex2f( x2, y2 );
      glVertex2f( x1, y2 );
   glEnd();

   //Draw outside
   glLineWidth( 2 );
      glColor3fv( colors[shapeColor] );
   glBegin( GL_LINE_LOOP );
      glVertex2f( x1, y1 );
      glVertex2f( x2, y1 );
      glVertex2f( x2, y2 );
      glVertex2f( x1, y2 );
   glEnd();
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will tell you if a filled rectangle contains a point
 *
 * @param[in] p - point to check the location of
 *
 * @return - a bool telling you if the point is in the shape
 *
 *****************************************************************************/
bool FilledRectangle::contains(Point p)
{
   //height and width are positive
   if(height > 0 && width > 0 )
   {
      if(p.returnX() < (location.returnX() + width) &&
        (p.returnX() > location.returnX()) &&
        (p.returnY() < (location.returnY() + height)) &&
        (p.returnY() > location.returnY()))
        {
           return true;
        }
   }

   //height is positive & width is negative
   else if(height > 0 && width < 0 )
   {
      if(p.returnX() > (location.returnX() + width) &&
        (p.returnX() < location.returnX()) &&
        (p.returnY() < (location.returnY() + height)) &&
        (p.returnY() > location.returnY()))
        {
           return true;
        }
   }

   //height is negative & width is positive
   else if(height < 0 && width > 0)
   {
      if(p.returnX() < (location.returnX() + width) &&
        (p.returnX() > location.returnX()) &&
        (p.returnY() > (location.returnY() + height)) &&
        (p.returnY() < location.returnY()))
        {
           return true;
        }
   }

   //both height and width are negative
   else
   {
      if(p.returnX() > (location.returnX() + width) &&
        (p.returnX() < location.returnX()) &&
        (p.returnY() > (location.returnY() + height)) &&
        (p.returnY() < location.returnY()))
        {
           return true;
        }
   }

   //return false if it doesn't meet the criteria
   return false;
}

/**************************************************************************//**
 * @author Dalton Baker
 *
 * @par Description:
 * This will return the fill color of a filled rectangle
 *
 * @return - the index to the fill color
 *
 *****************************************************************************/
int FilledRectangle::getFillColor()
{
   return fillColor;
}

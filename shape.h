/**************************************************************************//**
 * @file
 * @brief Header file to be used with Shape.cpp
 *****************************************************************************/
#ifndef __SHAPE__H__
#define __SHAPE__H__

#include "point.h"
#include <GL/freeglut.h>
#include <cmath>
#include <iostream>

/*!
* @brief This holds all the colors in the pallet and a few extras
*        black = 0, white = 1, grey = 2 & 3 These are for the menu
*/
const float colors[][3] = { { 0.0, 0.0, 0.0 }, { 1.0, 1.0, 1.0 },
                            { 0.5, 0.5, 0.5 }, { 0.25, 0.25, 0.25 },
                            //bright versions
                            { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 },
                            { 0.0, 0.0, 1.0 }, { 1.0, 1.0, 0.0 },
                            { 0.0, 1.0, 1.0 }, { 1.0, 0.0, 1.0 },
                            { 0.5, 0.0, 1.0 }, { 0.0, 0.5, 1.0 },
                            { 0.5, 1.0, 0.0 }, { 0.0, 1.0, 0.5 },
                            //darker versions
                            { 0.5, 0.0, 0.0 }, { 0.0, 0.5, 0.0 },
                            { 0.0, 0.0, 0.5 }, { 0.5, 0.5, 0.0 },
                            { 0.0, 0.5, 0.5 }, { 0.5, 0.0, 0.5 },
                            { 0.25, 0.0, 0.5 }, { 0.0, 0.25, 0.5 },
                            { 0.25, 0.5, 0.0 }, { 0.0, 0.5, 0.25 } };

/*!
* @brief A base class for all shape types
*/
class Shape
{
protected:
   /*! The point the shape will be drawn from */
   Point location;
   /*! A float to hold the shape height */
   float height;
   /*! A float to hold the shape width */
   float width;
   /*! A float to hold the shape color */
   int shapeColor;

public:
   Shape(Point p, float h, float w, int color);
   /*! A virtual destructof for all shapes */
   virtual ~Shape() = 0;
   Point getCoords();
   float getHeight();
   float getWidth();
   int getColor();
   void resize(float newH, float newW);
   void move(Point p);
   /*! A virtual function to return the fill color of a shape
    *  @return - the fill color of the shape                      */
   virtual int getFillColor() = 0;
   /*! A virtual function to draw any of the shapes */
   virtual void draw() = 0;
   /*! A virtual function to check if a point is inside a shape
    *  @param[in] p - the point we are checking the location of
    *  @return - true if the point is in the shape                */
   virtual bool contains(Point p) = 0;
};

#endif
